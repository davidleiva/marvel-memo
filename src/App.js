import React, { Component } from 'react';
import './App.scss';
import Starterpage from './components/Starterpage/Starterpage';
import Canvas from './components/Canvas/Canvas';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div id="App-container">
          <Starterpage></Starterpage>
          <Canvas></Canvas>
        </div>
      </div>
    );
  }
}

export default App;
