import React, { Component } from 'react';
import './Starterpage.component.scss';
import logo from './../../img/marvel-logo.png';

class Starterpage extends Component {
  constructor() {
    super();
    this.canvas = document.getElementsByClassName('canvas');
    this.starterel = document.getElementsByClassName('start-container');
  }

  startGame() {
    this.canvas[0].classList.remove('canvas--hidden');
    this.starterel[0].remove()
  };

  render() {
    return (
      <section className="start-container">
        <div className="start-container__caret">
          <div className="start-container__image">
            <img src={logo} alt="marvel-memo-logo" className="start-container__logo" />
          </div>
            <h1 className="start-container__header">MEMO</h1>
        </div>
        <button onClick={this.startGame.bind(this)} className="start-container__button">Play game</button>
      </section>
    );
  }
}

  export default Starterpage;