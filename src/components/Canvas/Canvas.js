import React, { Component } from 'react';
import './Canvas.component.scss';

class Canvas extends Component {

    constructor() {
        super();
        this.loading = document.getElementsByClassName('canvas__loading');
        this.container = document.getElementsByClassName('canvas__container');
        this.numberOfClicks = 0;
    }

    setScene(data) {
        let heroes = data.data.data.results;
        heroes = heroes.concat(heroes);
        heroes = heroes.sort(function() { return 0.5 - Math.random() });
        this.loading[0].remove();
        this.addHeroesToCanvas(heroes);
    }

    getHeroes() {
        fetch('http://gateway.marvel.com/v1/public/series/22547/characters?orderBy=-modified&limit=8&ts=0r81t&apikey=93fca11de04500292848b3eaf95bec08&hash=7dda8ddb50c06a2d89b984c1e1135aa1')
        .then(function(response) {
            return response.json();
        })
        .then(data => this.setScene({ data }));
    }

    addHeroesToCanvas(heroes) {
        for(let i = 0; i < heroes.length; i++) {
            let template = "<div class='card'><div class='card__face card--front'></div><div class='card__face card--back'><img class='card--image' src='{imageURL}' alt=' card'></div></div>";
            let image = heroes[i].thumbnail.path + '/portrait_uncanny.' + heroes[i].thumbnail.extension;
            template = template.replace('{imageURL}',image)
            let z = document.createElement('div');
            z.className = 'card__container';
            z.innerHTML = template;
            this.container[0].appendChild(z);
        }
        this.addClickEvent('card');
    }

    addClickEvent(elclass) {
        let list = document.getElementsByClassName(elclass);
        for (let i = 0, len = list.length; i < len; i++) {
            list[i].addEventListener('click', e => this.discoverCard(e));
        }
    }

    discoverCard(e) {
        let clickedCard = e.currentTarget;
        if (clickedCard.classList.contains('card--isflipped') === false && this.numberOfClicks < 2) {
            e.currentTarget.classList.add('card--isflipped');
            this.numberOfClicks++;
            this.checkStage(e);
        }
    }

    resetClicks() {
        this.numberOfClicks = 0;
    }

    checkStage(e) {
        var _this = this;
        let flippedCards = document.getElementsByClassName('card--isflipped');
        if(flippedCards.length === 2) {
            if(flippedCards[0].getElementsByTagName('img')[0].src === flippedCards[1].getElementsByTagName('img')[0].src) {
                setTimeout(
                    function(){
                        let flippedcards = document.getElementsByClassName('card--isflipped');
                        while (flippedcards.length) {
                            flippedcards[0].classList = 'card card--still';
                        }
                        _this.resetClicks();
                    },1000
                );
            } else {
                setTimeout(
                    function(){
                        let flippedcards = document.getElementsByClassName('card--isflipped');
                        while (flippedcards.length) {
                            flippedcards[0].classList = 'card';
                        }
                        _this.resetClicks();
                    }, 1000
                );
            }
        }
    }

    render() {
        return (
            <section className="canvas canvas--hidden">
                <div className="canvas__container">
                    <p className="canvas__loading">Loading</p>
                </div>
            </section>
        );
    }

    componentDidMount() {
        this.getHeroes();
    }
  }

  export default Canvas;